import { PubSub } from '../../src/pubsub';


test('PubSub: publishes string test', () => {
  const pubsub = new PubSub();
  pubsub.subscribe('test', () => 'test');

  expect(pubsub.publish('test', {})).toEqual(['test']);
});

test('PubSub: publishes number 4', () => {
  const pubsub = new PubSub();
  pubsub.subscribe('test', (data: any) => data.x * 2);

  expect(pubsub.publish('test', {x: 2})).toEqual([4]);
});

test('PubSub: publishes number empty list when no event to publish is matched', () => {
  const pubsub = new PubSub();
  pubsub.subscribe('test', (data: any) => data.x * 2);

  expect(pubsub.publish('no-match', {x: 2})).toEqual([]);
});

