/**
 * PubSub class
 * Transmits events from publisher to subscriber
 */
export class PubSub {
  private events: {};
  /**
   * @constructor - Intitialize event bus object
   */
  constructor() {
    this.events = {};
  }

  /**
   * Subscribe callback call on event
   * @param event {string} - Event key
   * @param callback {function} - Function to call on event
   */
  public subscribe(event: string, callback: Function) {
    if (!this.events.hasOwnProperty(event)) {
      this.events[event] = [];
    }

    return this.events[event].push(callback);
  }

  /**
   * Pusblish events by calling callback function with data as params
   * @param event {string} - Event key
   * @param data {object} - Data to be passed to callback
   */
  public publish(event: string, data: {}) {
    if (!this.events.hasOwnProperty(event)) {
      return [];
    }

    return this.events[event].map((callback: Function) => callback(data));
  }
}
