import { Store, StoreStatus } from '../store';

/**
 * Abstract Component base class.
 */
export abstract class Component {
  protected element: HTMLElement;

  /**
   * @param props {object} - Properties of component
   */
  constructor(props: any) {
    if (props.store instanceof Store) {
      props.store.events.subscribe(StoreStatus.CHANGE, () => this.render());
    }
    if (props.selector) {
      this.element = document.querySelector(props.selector);
    }
  }

  /**
   * Abstract render method.
   */
  public abstract render();
}


/**
 * Composite component.
 * Composite is used as container for other components and it calls their
 * render() functions.
 */
export class Composite extends Component {
  public components: Component[] = [];
  protected element: HTMLElement;

  constructor(props: any) {
    super(props);
  }

  public render() {
    for(const component of this.components) {
      component.render();
    }
  }
}
