import { PubSub } from '../pubsub';

/**
 * Store status states
 */
export enum StoreStatus {
  CHANGE = 'state-change',
  MUTATION = 'state-mutation',
  IDLE = 'state-idle',
  ACTION = 'state-action',
}

export interface IAction {
  [key: string]: Function
}

export interface IMutation {
  [key: string]: Function
}

export interface IState {
  [key: string]: any;
}

export interface IStoreParameters {
  state: IState;
  actions: IAction;
  mutations: IMutation;
}

export interface IStore {
  state: IState;
  actions: IAction;
  mutations: IMutation;
  status: StoreStatus;

  dispatch(actionKey: string, payload: any): boolean
  commit(mutationKey: string, payload: any): boolean
}


export class Store {
  public state: IState;
  private actions: IAction;
  private mutations: IMutation;
  private status: StoreStatus;
  private events: PubSub;

  /**
   * @constructor
   * @param parameters {object} - store setup parameters
   * @name actions {object} - contains functions which will call for mutation of store state
   * @name mutations {object} - contains functions which will modify store
   * @name events {PubSub} - contains instance of PubSub
   */
  constructor(parameters: IStoreParameters) {
    this.actions = parameters.actions;
    this.mutations = parameters.mutations;
    this.status = StoreStatus.IDLE;
    this.events = new PubSub();
    this.state = new Proxy<{}>(parameters.state !== {} ? parameters.state : {}, {
          get: (target: {}, p: string | number | symbol) => {
            return target[p];
          },
          set: (target: any, p: string | number | symbol, value: any) => {
            target[p] = value;

            this.events.publish(StoreStatus.CHANGE, this.state);

            if (this.status !== StoreStatus.MUTATION) {
              // tslint:disable-next-line
              console.warn(`You should use mutation to set ${p.toString()}`);
            }

            this.status = StoreStatus.IDLE;

            return true;
          }
      }
    );

    if (parameters.hasOwnProperty('actions')) {
      this.actions = parameters.actions;
    }
    if (parameters.hasOwnProperty('mutations')) {
      this.mutations = parameters.mutations;
    }
  }

  /**
   * Dispatch action to do mutation on store
   * @param actionKey {string}
   * @param payload {object}
   */
  public dispatch(actionKey: string, payload: any) {
    if (!this.actions[actionKey]) {
      // tslint:disable-next-line
      console.error(`Action: ${actionKey} does not exist`);

      this.events.publish(StoreStatus.CHANGE,  {});

      return false;
    }
    this.status = StoreStatus.ACTION;
    this.actions[actionKey](this, payload);
    this.events.publish(StoreStatus.CHANGE,  {});

    return true;
  }

  /**
   * Modifies store state by calling mutation function
   * @param mutationKey {string}
   * @param payload {any}
   */
  public commit(mutationKey: string, payload: any) {
    if (!this.mutations[mutationKey]) {
      // tslint:disable-next-line
      console.error(`Mutation ${mutationKey} does not exist`);

      return false
    }
    this.status = StoreStatus.MUTATION;
    const newState = this.mutations[mutationKey](this.state, payload);
    this.state = {...this.state, ...newState};

    return true
  }
}
