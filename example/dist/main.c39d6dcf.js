// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"../pubsub/index.ts":[function(require,module,exports) {
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * PubSub class
 * Transmits events from publisher to subscriber
 */

var PubSub =
/*#__PURE__*/
function () {
  /**
   * @constructor - Intitialize event bus object
   */
  function PubSub() {
    _classCallCheck(this, PubSub);

    this.events = {};
  }
  /**
   * Subscribe callback call on event
   * @param event {string} - Event key
   * @param callback {function} - Function to call on event
   */


  _createClass(PubSub, [{
    key: "subscribe",
    value: function subscribe(event, callback) {
      if (!this.events.hasOwnProperty(event)) {
        this.events[event] = [];
      }

      return this.events[event].push(callback);
    }
    /**
     * Pusblish events by calling callback function with data as params
     * @param event {string} - Event key
     * @param data {object} - Data to be passed to callback
     */

  }, {
    key: "publish",
    value: function publish(event, data) {
      if (!this.events.hasOwnProperty(event)) {
        return [];
      }

      return this.events[event].map(function (callback) {
        return callback(data);
      });
    }
  }]);

  return PubSub;
}();

exports.PubSub = PubSub;
},{}],"../src/store/index.ts":[function(require,module,exports) {
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var pubsub_1 = require("../pubsub");
/**
 * Store status states
 */


var StoreStatus;

(function (StoreStatus) {
  StoreStatus["CHANGE"] = "state-change";
  StoreStatus["MUTATION"] = "state-mutation";
  StoreStatus["IDLE"] = "state-idle";
  StoreStatus["ACTION"] = "state-action";
})(StoreStatus = exports.StoreStatus || (exports.StoreStatus = {}));

var Store =
/*#__PURE__*/
function () {
  /**
   * @constructor
   * @param parameters {object} - store setup parameters
   * @name actions {object} - contains functions which will call for mutation of store state
   * @name mutations {object} - contains functions which will modify store
   * @name events {PubSub} - contains instance of PubSub
   */
  function Store(parameters) {
    var _this = this;

    _classCallCheck(this, Store);

    this.actions = parameters.actions;
    this.mutations = parameters.mutations;
    this.status = StoreStatus.IDLE;
    this.events = new pubsub_1.PubSub();
    this.state = new Proxy(parameters.state !== {} ? parameters.state : {}, {
      get: function get(target, p) {
        return target[p];
      },
      set: function set(target, p, value) {
        target[p] = value;

        _this.events.publish(StoreStatus.CHANGE, _this.state);

        if (_this.status !== StoreStatus.MUTATION) {
          // tslint:disable-next-line
          console.warn("You should use mutation to set ".concat(p.toString()));
        }

        _this.status = StoreStatus.IDLE;
        return true;
      }
    });

    if (parameters.hasOwnProperty('actions')) {
      this.actions = parameters.actions;
    }

    if (parameters.hasOwnProperty('mutations')) {
      this.mutations = parameters.mutations;
    }
  }
  /**
   * Dispatch action to do mutation on store
   * @param actionKey {string}
   * @param payload {object}
   */


  _createClass(Store, [{
    key: "dispatch",
    value: function dispatch(actionKey, payload) {
      if (!this.actions[actionKey]) {
        // tslint:disable-next-line
        console.error("Action: ".concat(actionKey, " does not exist"));
        this.events.publish(StoreStatus.CHANGE, {});
        return false;
      }

      this.status = StoreStatus.ACTION;
      this.actions[actionKey](this, payload);
      this.events.publish(StoreStatus.CHANGE, {});
      return true;
    }
    /**
     * Modifies store state by calling mutation function
     * @param mutationKey {string}
     * @param payload {any}
     */

  }, {
    key: "commit",
    value: function commit(mutationKey, payload) {
      if (!this.mutations[mutationKey]) {
        // tslint:disable-next-line
        console.error("Mutation ".concat(mutationKey, " does not exist"));
        return false;
      }

      this.status = StoreStatus.MUTATION;
      var newState = this.mutations[mutationKey](this.state, payload);
      this.state = Object.assign({}, this.state, newState);
      return true;
    }
  }]);

  return Store;
}();

exports.Store = Store;
},{"../pubsub":"../pubsub/index.ts"}],"../src/component/index.ts":[function(require,module,exports) {
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var store_1 = require("../store");
/**
 * Abstract Component base class.
 */


var Component =
/**
 * @param props {object} - Properties of component
 */
function Component(props) {
  var _this = this;

  _classCallCheck(this, Component);

  if (props.store instanceof store_1.Store) {
    props.store.events.subscribe(store_1.StoreStatus.CHANGE, function () {
      return _this.render();
    });
  }

  if (props.selector) {
    this.element = document.querySelector(props.selector);
  }
};

exports.Component = Component;
/**
 * Composite component.
 * Composite is used as container for other components and it calls their
 * render() functions.
 */

var Composite =
/*#__PURE__*/
function (_Component) {
  _inherits(Composite, _Component);

  function Composite(props) {
    var _this2;

    _classCallCheck(this, Composite);

    _this2 = _possibleConstructorReturn(this, _getPrototypeOf(Composite).call(this, props));
    _this2.components = [];
    return _this2;
  }

  _createClass(Composite, [{
    key: "render",
    value: function render() {
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.components[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var component = _step.value;
          component.render();
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }]);

  return Composite;
}(Component);

exports.Composite = Composite;
},{"../store":"../src/store/index.ts"}],"store/actions.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Action functions which call for store state mutations
 */

exports.actions = {
  addItem: function addItem(context, payload) {
    context.commit('addItem', payload);
  },
  removeItem: function removeItem(context, payload) {
    context.commit('removeItem', payload);
  }
};
},{}],"store/mutations.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Mutation functions which modify store state
 */

exports.mutations = {
  addItem: function addItem(state, payload) {
    state.items.push(payload);
    return state;
  },
  removeItem: function removeItem(state, payload) {
    state.items.splice(payload, 1);
    return state;
  }
};
},{}],"store/state.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Initial state of the app
 */

exports.initial = {
  items: ['Learn HTML', 'Learn LESS', 'Learn TypeScript']
};
},{}],"store/index.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var actions_1 = require("./actions");

var mutations_1 = require("./mutations");

var state_1 = require("./state");

var store_1 = require("../../src/store");

exports.store = new store_1.Store({
  state: state_1.initial,
  actions: actions_1.actions,
  mutations: mutations_1.mutations
});
},{"./actions":"store/actions.ts","./mutations":"store/mutations.ts","./state":"store/state.ts","../../src/store":"../src/store/index.ts"}],"components/count.ts":[function(require,module,exports) {
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var component_1 = require("../../src/component");

var store_1 = require("../store");
/**
 * Displays number of items
 */


var CountComponent =
/*#__PURE__*/
function (_component_1$Componen) {
  _inherits(CountComponent, _component_1$Componen);

  function CountComponent(props) {
    _classCallCheck(this, CountComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(CountComponent).call(this, props));
  }

  _createClass(CountComponent, [{
    key: "render",
    value: function render() {
      this.element.innerHTML = "\n    <p>".concat(store_1.store.state.items.length, " items</p>\n    ");
    }
  }]);

  return CountComponent;
}(component_1.Component);

exports.CountComponent = CountComponent;
},{"../../src/component":"../src/component/index.ts","../store":"store/index.ts"}],"components/list.ts":[function(require,module,exports) {
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var component_1 = require("../../src/component");

var store_1 = require("../store");
/**
 * Displays list of items
 */


var ListComponent =
/*#__PURE__*/
function (_component_1$Componen) {
  _inherits(ListComponent, _component_1$Componen);

  function ListComponent(props) {
    _classCallCheck(this, ListComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(ListComponent).call(this, props));
  }

  _createClass(ListComponent, [{
    key: "render",
    value: function render() {
      if (store_1.store.state.items.length === 0) {
        this.element.innerHTML = '<p class="error">No items found</p>';
        return;
      }

      this.element.innerHTML = "\n    <ul>\n      ".concat(this.renderList(), "\n    </ul>\n    ");
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        var _loop = function _loop() {
          var button = _step.value;
          button.addEventListener('click', function () {
            store_1.store.dispatch('removeItem', button.id);
          });
        };

        for (var _iterator = this.element.querySelectorAll('button')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          _loop();
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: "renderList",
    value: function renderList() {
      var index = -1;
      return store_1.store.state.items.map(function (item) {
        index += 1;
        return "\n        <li>\n          ".concat(item, "\n          <button id=\"").concat(index, "\" aria-label=\"Delete this item\">x</button>\n        </li>\n      ");
      });
    }
  }]);

  return ListComponent;
}(component_1.Component);

exports.ListComponent = ListComponent;
},{"../../src/component":"../src/component/index.ts","../store":"store/index.ts"}],"components/status.ts":[function(require,module,exports) {
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var component_1 = require("../../src/component");

var store_1 = require("../store");
/**
 * Displays store items
 */


var StatusComponent =
/*#__PURE__*/
function (_component_1$Componen) {
  _inherits(StatusComponent, _component_1$Componen);

  function StatusComponent(props) {
    _classCallCheck(this, StatusComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(StatusComponent).call(this, props));
  }

  _createClass(StatusComponent, [{
    key: "render",
    value: function render() {
      this.element.innerHTML = "\n    <div>\n      ".concat(store_1.store.state.items, "\n    </div>\n    ");
    }
  }]);

  return StatusComponent;
}(component_1.Component);

exports.StatusComponent = StatusComponent;
},{"../../src/component":"../src/component/index.ts","../store":"store/index.ts"}],"components/index.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var count_1 = require("./count");

exports.CountComponent = count_1.CountComponent;

var list_1 = require("./list");

exports.ListComponent = list_1.ListComponent;

var status_1 = require("./status");

exports.StatusComponent = status_1.StatusComponent;
},{"./count":"components/count.ts","./list":"components/list.ts","./status":"components/status.ts"}],"main.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var component_1 = require("../src/component");

var store_1 = require("./store");

var components_1 = require("./components"); // no components scenario


var formElement = document.querySelector('#form');
var inputElement = document.querySelector('#new-item-input');
formElement.addEventListener('submit', function (evt) {
  evt.preventDefault();
  var value = inputElement.value.trim();

  if (value.length) {
    // use dispatch to add item
    store_1.store.dispatch('addItem', value);
    inputElement.value = '';
    inputElement.focus();
  }
}); // create root component

var app = new component_1.Composite({
  store: store_1.store,
  selector: '#app'
}); // add nested components

app.components = [new components_1.CountComponent({
  selector: '#count'
}), new components_1.ListComponent({
  selector: '#item-list'
}), new components_1.StatusComponent({
  selector: '#status'
})];
app.render();
},{"../src/component":"../src/component/index.ts","./store":"store/index.ts","./components":"components/index.ts"}],"C:/Users/josip/AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "51126" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["C:/Users/josip/AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","main.ts"], null)
//# sourceMappingURL=/main.c39d6dcf.map