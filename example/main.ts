import { Composite } from '../src/component';
import { store } from './store';
import { CountComponent, ListComponent, StatusComponent } from './components';


// no components scenario
const formElement: HTMLFormElement = document.querySelector('#form');
const inputElement: HTMLInputElement = document.querySelector('#new-item-input');

formElement.addEventListener('submit', (evt: Event) => {
  evt.preventDefault();

  const value = inputElement.value.trim();

  if(value.length) {
    // use dispatch to add item
    store.dispatch('addItem', value);
    inputElement.value = '';
    inputElement.focus();
  }
});


// create root component
const app = new Composite({
  store,
  selector: '#app'
});

// add nested components
app.components = [
  new CountComponent({
    selector: '#count',
  }),
  new ListComponent({
    selector: '#item-list'
  }),
  new StatusComponent({
    selector: '#status'
  })
];

app.render();
