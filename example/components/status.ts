import { Component } from '../../src/component';
import { store } from '../store';


/**
 * Displays store items
 */
export class StatusComponent extends Component {

  constructor(props: any) {
    super(props);
  }

  public render() {
    this.element.innerHTML = `
    <div>
      ${store.state.items}
    </div>
    `;
  }
}
