import { Component } from '../../src/component';
import { store } from '../store';


/**
 * Displays list of items
 */
export class ListComponent extends Component {

  constructor(props: any) {
    super(props);
  }

  public render() {
    if (store.state.items.length === 0) {
      this.element.innerHTML = '<p class="error">No items found</p>';

      return;
    }

    this.element.innerHTML = `
    <ul>
      ${this.renderList()}
    </ul>
    `;

    for(const button of this.element.querySelectorAll('button')) {
      button.addEventListener('click', () => {
        store.dispatch('removeItem', button.id);
      })
    }
  }

  private renderList() {
    let index = -1;

    return store.state.items.map((item: string) => {
      index += 1;

      return `
        <li>
          ${item}
          <button id="${index}" aria-label="Delete this item">x</button>
        </li>
      `;
    });
  }
}
