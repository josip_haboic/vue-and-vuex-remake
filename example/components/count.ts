import { Component } from '../../src/component';
import { store } from '../store';


/**
 * Displays number of items
 */
export class CountComponent extends Component {
  constructor(props: any) {
    super(props);
  }

  public render() {
    this.element.innerHTML = `
    <p>${store.state.items.length} items</p>
    `
  }
}
