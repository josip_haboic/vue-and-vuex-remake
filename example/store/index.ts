import { actions } from './actions';
import { mutations } from './mutations';
import { initial } from './state';
import { Store } from '../../src/store';



export const store = new Store({
  state: initial,
  actions: actions,
  mutations: mutations
});

