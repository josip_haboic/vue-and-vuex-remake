import { IMutation } from '../../src/store';


/**
 * Mutation functions which modify store state
 */
export const mutations: IMutation = {
  addItem: (state: any, payload: string) => {
    state.items.push(payload);

    return state;
  },
  removeItem: (state: any, payload: number) => {
    state.items.splice(payload, 1);

    return state;
  }
}
