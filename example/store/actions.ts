import { Store, IAction } from '../../src/store';


/**
 * Action functions which call for store state mutations
 */
export const actions: IAction = {
  addItem: (context: Store, payload: string) => {
    context.commit('addItem', payload);
  },

  removeItem: (context: Store, payload: number) => {
    context.commit('removeItem', payload);
  }
}
