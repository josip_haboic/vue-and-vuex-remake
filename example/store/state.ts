import { IState } from '../../src/store';


/**
 * Initial state of the app
 */
export const initial: IState = {
  items: [
    'Learn HTML',
    'Learn LESS',
    'Learn TypeScript',
  ]
};
